const express = require('express');
const Event = require('../models/Event');
const auth = require('../middlewares/auth');

const createRouter = () => {
    const router = express.Router();

    router.get('/', auth, async (req, res) => {
        const user = req.user;

        const events = await Event.find({author: user._id});
        if(!events) {
            res.send({message: 'Events not found!'});
        }

        res.send(events);
    });

    router.post('/addEvent', auth, (req, res) => {
        const eventData = req.body;
        eventData.author = req.user._id;

        const event = new Event(eventData);

        event.save().then(event => res.send(event));
    });


    return router;
};

module.exports = createRouter;