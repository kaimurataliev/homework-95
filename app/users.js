const express = require('express');
const User = require('../models/User');
const config = require('../config');
const request = require('request-promise-native');
const auth = require('../middlewares/auth');
const nanoid = require('nanoid');
const createRouter = () => {
    const router = express.Router();

    // Регистрация и логин через Facebook
    router.post('/facebookLogin', async (req, res) => {

        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`

        try {
            const response = await request(debugTokenUrl);
            const decodedResponse = JSON.parse(response);

            if(decodedResponse.data.hasOwnProperty('error')) {
                return res.status(401).send({message: 'Facebook token incorrect'});
            }

            if(req.body.id !== decodedResponse.data.user_id) {
                return res.status(401).send({message: 'Wrong user ID'});
            }

            let user = await User.findOne({facebookId: req.body.id});

            if(!user) {
                user = new User({
                    email: req.body.email,
                    password: nanoid(10),
                    facebookId: req.body.id,
                    avatar: req.body.picture.data.url
                });

                user.token = user.generateToken();

                console.log(user)
                await user.save();
            }

            return res.send(user);

        } catch (error) {
            return  res.status(500).send({message: 'Internal error'});
        }

    });

    router.put('/addFriend', auth, async (req, res) =>{
        const friendData = req.body.email;
        if(!friendData) {
            res.send({message: 'No email presented'});
        }
        const id = req.user._id;

        const friend = await User.findOne({email: friendData});
        if(!friend) {
            res.send({message: 'User not found!'});
        }
        const user = User.findOneAndUpdate({_id: id}, {$push: {friends: friend._id}});
        await user.save();
    });


    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();
        await user.save();

        return res.send(success);
    });

    return router;
};

module.exports = createRouter;