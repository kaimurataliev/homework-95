const mongoose = require('mongoose');
const config = require('../config');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    startDate: {
        type: String
    },
    endDate: {
        type: String
    },
    text: {
        type: String, required: true
    },
    title: {
        type: String
    }
});

const Event = mongoose.model('Event', EventSchema);

module.exports = Event;