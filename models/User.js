const mongoose = require('mongoose');
const config = require('../config');
const SALT_WORK_FACTOR = 10;
const bcrypt = require('bcryptjs');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String, required: true
    },
    password: {
        type: String, required: true, unique: true
    },
    token: {
        type: String
    },
    role: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    facebookId: {
        type: String,
        unique: true
    },
    friends: [{type: Schema.Types.ObjectId, ref: 'User'}]
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSaltSync(SALT_WORK_FACTOR);
    this.password = await bcrypt.hashSync(this.password, salt);

    next();
});

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.generateToken = function () {
    return nanoid(20);
};

const User = mongoose.model('User', UserSchema);

module.exports = User;